function [forwBackVel, leftRightVel, rotVel, finish] = solution1b(...
        pts, contacts, position, orientation, varargin ...
        )
    % Initialize variables
    MAX_VEL_T = 3;
    KP_VEL_R = 10; MAX_VEL_R = 2;
    KP_ROT_VEL = 10; MAX_ROT_VEL = 1;
    % Initialize the robot control variables (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    switch length(varargin)
        case 0
            error('At least the circle radius must be specified');
        case 1
            radius = varargin{1};
            center_x = 1; center_y = 1; goal_att = 0;
        case 2
            error('Both x and y positions of the circle center are required')
        case 3
            [radius, center_x, center_y] = deal(varargin{:});
            goal_att = 0;
        case 4
            [radius, center_x, center_y, goal_att] = deal(varargin{:});
            goal_att = wrapToPi(goal_att);
        otherwise
            error('Too many input arguments')
    end

    center = [center_x, center_y];
    pos = position(1:2);
    att = orientation(3);

    % Finite State Machine initialization
    [INIT, MOVE] = assign_states();
    persistent state;

    if isempty(state)
        state = INIT;
    end

    % Finite State Machine code
    switch state
        case INIT
            state = MOVE;
            disp('Start moving now')
        case MOVE
            % Radius error
            circle_pos = center - pos;
            circle_dist = norm(circle_pos);
            radial_direction = circle_pos / circle_dist;
            % Always clockwise turning
            tangential_direction = radial_direction * [0 1; -1 0];
            % Fixed tangential velocity
            vel_t = MAX_VEL_T;
            % Calculate required radial velocity
            radius_err = circle_dist - radius;
            P = radius_err * KP_VEL_R;
            vel_r = max(min(P, MAX_VEL_R), -MAX_VEL_R);
            vel_G = vel_r * radial_direction + vel_t * tangential_direction;
            vel_L = global2local(vel_G, att);
            leftRightVel = vel_L(1);
            forwBackVel = vel_L(2);

            % Difference in attitude
            err_att = angdiff(goal_att, att);
            % Compute the necessary rotational velocity
            P = err_att * KP_ROT_VEL;
            rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);
        otherwise
            error('Unknown state %s.\n', state);
    end

end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end

function vel_L = global2local(vel_G, theta)
    % vel_G: Desired velocity expressed in the global frame
    % theta: Get current rotation of the robot

    % Calculate rotation matrix - transformation from the global frame
    % to the local frame of the robot
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];

    % Desired velocity expressed in the local frame
    vel_L = vel_G * R;
end
