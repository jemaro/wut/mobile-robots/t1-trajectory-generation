function [forwBackVel, leftRightVel, rotVel, finish] = solution1a(...
        pts, contacts, position, orientation, varargin ...
        )
    % Initialize variables
    ERR_THRESHOLD = 1e-3;
    KP_VEL = 100; MAX_VEL = 3;
    KP_ROT_VEL = 100; MAX_ROT_VEL = 1;
    % Initialize the robot control variables (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    if length(varargin) ~= 3
        error('Wrong number of additional arguments: %d\n', length(varargin));
    end

    [goal_x, goal_y, goal_att] = deal(varargin{:});
    goal_att = wrapToPi(goal_att);
    goal_pos = [goal_x, goal_y];
    pos = position(1:2);
    att = orientation(3);

    % Finite State Machine initialization
    [INIT, MOVE, FINISH] = assign_states();
    persistent state;

    if isempty(state)
        state = INIT;
    end

    % Finite State Machine code
    switch state
        case INIT
            state = MOVE;
            disp('Start moving now')
        case MOVE
            % Difference in position
            diff_pos = goal_pos - pos;
            err_pos = norm(diff_pos);
            goal_direction = diff_pos / err_pos;
            % Difference in attitude
            err_att = angdiff(goal_att, att);

            % Completition condition
            if abs(err_pos) < ERR_THRESHOLD && abs(err_att) < ERR_THRESHOLD
                state = state + 1;
            end

            % Calculate the necessary linear velocity
            P = err_pos * KP_VEL;
            vel_G = max(min(P, MAX_VEL), -MAX_VEL) * goal_direction;
            vel_L = global2local(vel_G, att);
            leftRightVel = vel_L(1);
            forwBackVel = vel_L(2);
            % Compute the necessary rotational velocity
            P = err_att * KP_ROT_VEL;
            rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);
        case FINISH
            finish = true;
        otherwise
            error('Unknown state %s.\n', state);
    end

end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end

function vel_L = global2local(vel_G, theta)
    % vel_G: Desired velocity expressed in the global frame
    % theta: Get current rotation of the robot

    % Calculate rotation matrix - transformation from the global frame
    % to the local frame of the robot
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];

    % Desired velocity expressed in the local frame
    vel_L = vel_G * R;
end
