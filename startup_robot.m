% Initializer function that will only work if
% https://github.com/RCPRG-ros-pkg/emor_trs is cloned in the parent directory
function startup_robot()
    run('../emor_trs/matlab/startup_robot.m');
    addpath('../emor_trs/youbot');