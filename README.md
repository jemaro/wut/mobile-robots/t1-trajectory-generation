# T1 Trajectory generation

Please, consider [reading this report
online](https://gitlab.com/jemaro/wut/mobile-robots/t1-trajectory-generation/-/blob/master/README.md)
in order to be able to see the results animations.

### [Andreu Gimenez Bolinches](01163563@pw.edu.pl), student number: 317486

The main goal of the first tutorial is to learn to generate trajectories for a mobile robot.

## Task 1.a: Following a trajectory to the destination point

The first trajectory is a straight line to the input destination (first and
second input arguments) and a rotation until reaching the input
orientation (third argument in radians). Two independent regulators
are built, one for the position and one for the orientation of the robot. The
competition condition is to have a lower position and orientation than a
threshold.

```matlab
function [forwBackVel, leftRightVel, rotVel, finish] = solution1a(...
        pts, contacts, position, orientation, varargin ...
        )
    % Initialize variables
    ERR_THRESHOLD = 1e-3;
    KP_VEL = 100; MAX_VEL = 3;
    KP_ROT_VEL = 100; MAX_ROT_VEL = 1;
    % Initialize the robot control variables (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    if length(varargin) ~= 3
        error('Wrong number of additional arguments: %d\n', length(varargin));
    end

    [goal_x, goal_y, goal_att] = deal(varargin{:});
    goal_att = wrapToPi(goal_att);
    goal_pos = [goal_x, goal_y];
    pos = position(1:2);
    att = orientation(3);

    % Finite State Machine initialization
    [INIT, MOVE, FINISH] = assign_states();
    persistent state;

    if isempty(state)
        state = INIT;
    end

    % Finite State Machine code
    switch state
        case INIT
            state = MOVE;
            disp('Start moving now')
        case MOVE
            % Difference in position
            diff_pos = goal_pos - pos;
            err_pos = norm(diff_pos);
            goal_direction = diff_pos / err_pos;
            % Difference in attitude
            err_att = angdiff(goal_att, att);

            % Completition condition
            if abs(err_pos) < ERR_THRESHOLD && abs(err_att) < ERR_THRESHOLD
                state = state + 1;
            end

            % Calculate the necessary linear velocity
            P = err_pos * KP_VEL;
            vel_G = max(min(P, MAX_VEL), -MAX_VEL) * goal_direction;
            vel_L = global2local(vel_G, att);
            leftRightVel = vel_L(1);
            forwBackVel = vel_L(2);
            % Compute the necessary rotational velocity
            P = err_att * KP_ROT_VEL;
            rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);
        case FINISH
            finish = true;
        otherwise
            error('Unknown state %s.\n', state);
    end

end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end

function vel_L = global2local(vel_G, theta)
    % vel_G: Desired velocity expressed in the global frame
    % theta: Get current rotation of the robot

    % Calculate rotation matrix - transformation from the global frame
    % to the local frame of the robot
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];

    % Desired velocity expressed in the local frame
    vel_L = vel_G * R;
end
```

### Results

```
run_simulation(@solution1a, false, -1, 2, -pi/4)
```

![solution1a_1 animation](solution1a_1.gif)
![solution1a_1 screenshot](solution1a_1.png)

```
run_simulation(@solution1a, false, 3, 2, -4*pi/6)
```

![solution1a_2 animation](solution1a_2.gif)
![solution1a_2 screenshot](solution1a_2.png)

In addition to this two recorded tests, the control algorithm has also been
successfully tested in the edge scenarios where the goal orientation is close
to `pi` or `2*pi`:

```
run_simulation(@solution1a, false, 1, 2, pi)
```

```
run_simulation(@solution1a, false, 1, 2, 2*pi-0.01) 
```

## Task 1.b: Moving along a circle trajectory with constant orientation

The second trajectory is a circle with a constant orientation. The first
argument is required and it is the circle radius. The second and third
arguments are the circle center, which default to [1 1]. Being the second
argument the x axis value and the third argument the y axis value. The last and
forth argument is the orientation in radians. Which defaults to 0, no
orientation changes with respect to the initial position.

```matlab
function [forwBackVel, leftRightVel, rotVel, finish] = solution1b(...
        pts, contacts, position, orientation, varargin ...
        )
    % Initialize variables
    MAX_VEL_T = 3;
    KP_VEL_R = 10; MAX_VEL_R = 2;
    KP_ROT_VEL = 10; MAX_ROT_VEL = 1;
    % Initialize the robot control variables (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    switch length(varargin)
        case 0
            error('At least the circle radius must be specified');
        case 1
            radius = varargin{1};
            center_x = 1; center_y = 1; goal_att = 0;
        case 2
            error('Both x and y positions of the circle center are required')
        case 3
            [radius, center_x, center_y] = deal(varargin{:});
            goal_att = 0;
        case 4
            [radius, center_x, center_y, goal_att] = deal(varargin{:});
            goal_att = wrapToPi(goal_att);
        otherwise
            error('Too many input arguments')
    end

    center = [center_x, center_y];
    pos = position(1:2);
    att = orientation(3);

    % Finite State Machine initialization
    [INIT, MOVE] = assign_states();
    persistent state;

    if isempty(state)
        state = INIT;
    end

    % Finite State Machine code
    switch state
        case INIT
            state = MOVE;
            disp('Start moving now')
        case MOVE
            % Radius error
            circle_pos = center - pos;
            circle_dist = norm(circle_pos);
            radial_direction = circle_pos / circle_dist;
            % Always clockwise turning
            tangential_direction = radial_direction * [0 1; -1 0];
            % Fixed tangential velocity
            vel_t = MAX_VEL_T;
            % Calculate required radial velocity
            radius_err = circle_dist - radius;
            P = radius_err * KP_VEL_R;
            vel_r = max(min(P, MAX_VEL_R), -MAX_VEL_R);
            vel_G = vel_r * radial_direction + vel_t * tangential_direction;
            vel_L = global2local(vel_G, att);
            leftRightVel = vel_L(1);
            forwBackVel = vel_L(2);

            % Difference in attitude
            err_att = angdiff(goal_att, att);
            % Compute the necessary rotational velocity
            P = err_att * KP_ROT_VEL;
            rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);
        otherwise
            error('Unknown state %s.\n', state);
    end

end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end

function vel_L = global2local(vel_G, theta)
    % vel_G: Desired velocity expressed in the global frame
    % theta: Get current rotation of the robot

    % Calculate rotation matrix - transformation from the global frame
    % to the local frame of the robot
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];

    % Desired velocity expressed in the local frame
    vel_L = vel_G * R;
end
```

### Results

```
run_simulation(@solution1b, false, 1)
```

![solution1b_1 animation](solution1b_1.gif)
![solution1b_1 screenshot](solution1b_1.png)

```
run_simulation(@solution1b, false, 0.5, -1, 2, pi/2)
```

![solution1b_2 animation](solution1b_2.gif)
![solution1b_2 screenshot](solution1b_2.png)

In addition to this two recorded tests, the control algorithm has also been
successfully tested in the edge scenarios where the goal orientation is close
to `pi` or `2*pi`:

```
run_simulation(@solution1b, false, 0.5, -1, 2, pi)
```
```
run_simulation(@solution1b, false, 0.8, 1, 2, -0.001)
```